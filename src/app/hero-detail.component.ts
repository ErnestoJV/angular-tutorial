import { Component, Input } from '@angular/core';

import { Hero } from './hero';


@Component({
    selector : 'my-hero-detail',
    template : `
        <section *ngIf="hero">
            <h1>{{ hero.name }} details!</h1>
            <div>
                <label>id: </label>{{ hero.id }}
            </div>
            <div>
                <label>name: </label>
                <input type="text" [(ngModel)]="hero.name" placeholder="name">
            </div>
        </section>
    `
})
export class HeroDetailComponent{
    @Input()
    hero : Hero;
}

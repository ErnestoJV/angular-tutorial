import { Component, OnInit } from '@angular/core';

import { Hero } from './hero';
import { HeroService } from './hero.service';



@Component({
    selector : 'my-heroes',
    styleUrls : ['./app.component.css'],
    template : `
        <h2>My Heroes</h2>
        <ul class="heroes">
            <li
                [class.selected]="hero === selectedHero"
                *ngFor="let hero of heroes"
                (click)="onSelect(hero)">
                <span class="badge">{{ hero.id }}</span> {{ hero.name }}
            </li>
        </ul>

        <my-hero-detail [hero]="selectedHero"></my-hero-detail>
    `,
    providers : [ HeroService ]

})
export class HeroesComponent implements OnInit {

    title = 'Tour of heroes';
    selectedHero : Hero;
    heroes : Hero[];


    constructor(private heroService: HeroService){}

    ngOnInit() : void {
        this.getHeroes();
    }


    getHeroes() : void {
        this.heroService.getHeroes().then(heroes => this.heroes = heroes);
    }


    onSelect(hero : Hero) {
        this.selectedHero = hero;
    }

}
